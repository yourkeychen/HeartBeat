<%--
 * 
 * @author Shengzhao Li
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>用户注册成功</title>
</head>
<body>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-success">
            <div>
                <h3><em class="fui-check-circle"></em> 祝贺!!</h3>
                注册成功
                <br/>
                <br/>
                <a href="${contextPath}/login.hb">去登录</a>
            </div>
        </div>
    </div>
</div>


</body>
</html>